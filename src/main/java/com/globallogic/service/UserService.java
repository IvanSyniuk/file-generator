package com.globallogic.service;

import com.globallogic.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> getUserById(Long id);

    List<User> getAllUser();

    void deleteUser(Long id);
}
